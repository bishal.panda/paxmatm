package com.iserveu.paxmatm.View.MPOS;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.iserveu.paxmatm.Adapters.Adapter.QuickPaymentAdapter;
import com.iserveu.paxmatm.Interface.MPOS.PosApiInterface;
import com.iserveu.paxmatm.Model.MPOS.PosTransResponse;
import com.iserveu.paxmatm.R;
import com.iserveu.paxmatm.Service.BankResponse;
import com.iserveu.paxmatm.Service.DataSetting;
import com.iserveu.paxmatm.Service.PaxConditional;
import com.iserveu.paxmatm.Service.PosServices;
import com.iserveu.paxmatm.Service.TempApiAuthFactory;
import com.iserveu.paxmatm.Utils.PAXScreen;
import com.iserveu.paxmatm.Utils.ResponseConstant;
import com.iserveu.paxmatm.Utils.ResultEvent;
import com.iserveu.paxmatm.Utils.Tools;
import com.iserveu.paxmatm.Utils.TransactionResponseHandler;
import com.iserveu.paxmatm.View.ChooseCard.ChooseCardActivity;
import com.iserveu.paxmatm.View.Dashboard.MainActivity;
import com.iserveu.paxmatm.View.Error.ErrorActivity;
import com.iserveu.paxmatm.View.transaction_report.TransactionStatusActivity;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.api.ResponseCode;
import com.paxsz.easylink.device.DeviceInfo;
import com.paxsz.easylink.model.DataModel.DataType;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.iserveu.paxmatm.Service.PosServices.completeTransAndGetData;
import static com.iserveu.paxmatm.Service.PosServices.disTransFailed;
import static com.iserveu.paxmatm.Service.PosServices.setResponseData;

public class PosActivity extends AppCompatActivity implements View.OnClickListener {

    public EditText amountText;
    public Button transSubmit;
    public String amount = "";
    public Boolean fallBack = false;

    public EasyLinkSdkManager manager;
    private Handler handler;
    public PosServices posServices;

    private ExecutorService backgroundExecutor;
    int SetResponseCodeRes = 99999;
    private RecyclerView listQuickpayment;
    QuickPaymentAdapter quickPaymentAdapter;
    ArrayList<String>quickPaymentList = new ArrayList<>();
    private Button btnWithdraw,btn_enquiry;

    private Boolean withdraw_bool=false,balance_enq=false;
    String transaction_type="00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pos);

        posServices = new PosServices();

        register(this);
        manager = EasyLinkSdkManager.getInstance(this);
        handler = new Handler();
        backgroundExecutor = Executors.newFixedThreadPool(10, runnable -> {
            Thread thread = new Thread(runnable, "Background executor service");
            thread.setPriority(Thread.MIN_PRIORITY);
            thread.setDaemon(true);
            return thread;
        });

        listQuickpayment = findViewById(R.id.listQuickpayment);

        quickPaymentAdapter = new QuickPaymentAdapter(quickPaymentList,PosActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(),3);
        listQuickpayment.setLayoutManager(mLayoutManager);
        listQuickpayment.setItemAnimator(new DefaultItemAnimator());
        listQuickpayment.setAdapter(quickPaymentAdapter);
       //Adding static fdata.
        quickPaymentList.add("100");
        quickPaymentList.add("200");
        quickPaymentList.add("300");
        quickPaymentList.add("500");
        quickPaymentList.add("1000");
        quickPaymentList.add("2000");
        quickPaymentList.add("3000");
        quickPaymentList.add("5000");
        quickPaymentList.add("10000");



        amountText = findViewById(R.id.posAmount);
        transSubmit = findViewById(R.id.doTransaction);

        transSubmit.setOnClickListener(v -> {
            amount = "";
            amount = String.valueOf(amountText.getText());
            if(Integer.parseInt(amount) > 0){

                if(manager.isConnected(DeviceInfo.CommType.BLUETOOTH)){
                    runInBackground(() -> PosActivity.this.cardTransaction(amount));
                }
                else{
                    Toast.makeText(getApplicationContext(),"Please go to setting and connect your bluetooth device.",Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"Please enter a valid amount",Toast.LENGTH_LONG).show();
            }
        });


        btnWithdraw = findViewById(R.id.btnWithdraw);
        btnWithdraw.setOnClickListener(this);
        btn_enquiry = findViewById(R.id.btn_enquiry);
        btn_enquiry.setOnClickListener(this);

        //--------------------------------
       // InitPaymentView();

    }
    public void updateQuikPaymentAdapter(String value){
        amountText.setText(value);
    }
    public void cardTransaction(String amount){
        //Starting the Card Showing Activity
        Intent cardShowActivity = new Intent(getApplicationContext(), ChooseCardActivity.class);
        startActivity(cardShowActivity);

        manager.setDebugMode(true);
        int getResponse;
        Integer transPrepareRet = transPrepare();

        if(transPrepareRet == 0){
            int response = manager.startTransaction(reportedData -> {
                if(reportedData!=null){
                    Log.d("MPOS","Report Data : " + new String(reportedData));
                    PosServices.doEvent(new ResultEvent(ResultEvent.Status.RECV_REPORT, new String(reportedData)));
                }
                return new byte[0];
            });

            Log.d("MPOS","Start Transaction : " + response);
            
            if(response != ResponseCode.EL_RET_OK){

                switch (response){
                    case 4046:
                        PAXScreen.showErrorOnPax(manager,response);
                        goToErrorPage(response);
                        break;
                    case 4001:
                        PAXScreen.showErrorOnPax(manager,response);
                        goToErrorPage(response);
                        break;
                    case 4003:
                        PAXScreen.showErrorOnPax(manager,response);
                        goToErrorPage(response);
                        break;
                         default:
                             goToErrorPage(response);
                }

                //Rajesh
                // error handle here......... 4 condintions shows in pax , all shows in app.
                return;
            }

            //get card type
            ByteArrayOutputStream getCardTypeTags = new ByteArrayOutputStream();
            getResponse = manager.getData(DataType.CONFIGURATION_DATA,new byte[]{0x03, 0x01}, getCardTypeTags);
            Log.d("MPOS", "get card type: ret = " + getResponse + "  " + Tools.cleanByte(Tools.bcd2Str(getCardTypeTags.toByteArray())));
            if (getResponse != ResponseCode.EL_RET_OK) {
                PosServices.errorHandel(getResponse,getApplicationContext());
                PosServices.disTransFailed(getResponse);
                return;
            }

            Integer ret = transFlow(getCardTypeTags);
            if (ret != ResponseCode.EL_RET_OK) {
                ResponseCode.getRespCodeMsg(ret);
                PosServices.disTransFailed(ret);
                return;
            }
            //complete transaction success
            PosServices.doEvent(new ResultEvent(ResultEvent.Status.SUCCESS));

        }
    }

    private void goToErrorPage(Integer response) {
        Intent intent = new Intent(PosActivity.this, ErrorActivity.class);
        intent.putExtra("errorResponse",response);
        startActivity(intent);
    }

    private int transPrepare(){
        String hexAmt = Tools.decimalToHexaAmount(amount,10);
        String finalTransactionData = "";
        if(balance_enq == true){
             finalTransactionData = Tools.finalBalanceTranData("000000000000");
             balance_enq = false;

        }else{
             finalTransactionData = Tools.finalTranData(hexAmt);
        }

        Log.d("MPOS","Transaction Data : " + finalTransactionData);

        byte[] data = Tools.str2Bcd(finalTransactionData);
        ByteArrayOutputStream failedTags = new ByteArrayOutputStream();
        int transDataSetRet = manager.setData(DataType.TRANSACTION_DATA,data,failedTags);
        int confgDataSetRet;
        confgDataSetRet = DataSetting.setAllData(manager);

        Log.d("MPOS","Set Data Transaction: " + transDataSetRet);
        Log.d("MPOS","Set Data Configuration: " + confgDataSetRet);
        if (confgDataSetRet != ResponseCode.EL_RET_OK && transDataSetRet != ResponseCode.EL_RET_OK) {
            disTransFailed(ResponseCode.EL_TRANS_RET_EMV_DENIAL);
            return ResponseCode.EL_TRANS_RET_EMV_DENIAL;

        }
        else {
            return ResponseCode.EL_RET_OK;
        }

    }

    private int transFlow(ByteArrayOutputStream cardType) {
        if ((cardType.toByteArray()[cardType.size() - 1] == 1) || (cardType.toByteArray()[cardType.size() - 1] == 2)) {
            //MSR card
            return goMSRBranch();
        } else {
            //not MSR card
            return goNotMSRBranch();
        }
    }

    private int goMSRBranch() {

        online(manager,"msr");

        return 0;
    }

    private int goNotMSRBranch() {
        Integer result = null;
        String cardProcessingRes = DataSetting.CardProcessingResult(manager);
        Log.d("MPOS","Card Processing Result"+ DataSetting.CardProcessingResult(manager));
        switch(cardProcessingRes){
            case "02":
                online(manager,"chip");
            case "01":
                Toast.makeText(getApplicationContext(),"Transaction Approved",Toast.LENGTH_LONG).show();
                result = posServices.offline(manager);
                if (result != ResponseCode.EL_RET_OK) {
                    return result;
                }
                else {
                    return completeTransAndGetData(manager);
                }
            case "00":
                Toast.makeText(getApplicationContext(),"Transaction Declined",Toast.LENGTH_LONG).show();
                result = ResponseCode.EL_TRANS_RET_TRASN_DECLINED;
                if (result != ResponseCode.EL_RET_OK) {
                    return result;
                }
                break;
        }
        return result;
    }

    public void finish() {
        if(ChooseCardActivity.instance!=null){
            ChooseCardActivity.instance.finish();
        }else{
            Intent i = new Intent(PosActivity.this, MainActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

    }

    public void register(Object obj) {
        if (!EventBus.getDefault().isRegistered(obj)) {
            EventBus.getDefault().register(obj);
        }
    }

    public void unregister(Object obj) {
        if (EventBus.getDefault().isRegistered(obj)) {
            EventBus.getDefault().unregister(obj);
        }
    }

    public void runInBackground(final Runnable runnable) {
        backgroundExecutor.submit(runnable);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregister(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnWithdraw:
                btnWithdraw.setTextColor(Color.parseColor("#FFFFFF"));
                btn_enquiry.setTextColor(Color.parseColor("#0367D3"));
                btnWithdraw.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_check));
                btn_enquiry.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_uncheck));
                withdraw_bool=true;
                transaction_type="00";
                break;
            case R.id.btn_enquiry:
                btnWithdraw.setTextColor(Color.parseColor("#0367D3"));
                btn_enquiry.setTextColor(Color.parseColor("#FFFFFF"));
                btnWithdraw.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_uncheck));
                btn_enquiry.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.button_check));
                transaction_type="01";
                balance_enq = true;
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ResultEvent event) {
        switch ((ResultEvent.Status) event.getStatus()) {
            case SUCCESS:
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
                //transSuccess();
                break;
            case FAILED:
                finish();
                Toast.makeText(getApplicationContext(),ResponseCode.getRespCodeMsg((int) event.getData()),Toast.LENGTH_LONG);
                break;
            default:
                break;
        }
    }


    public void online(EasyLinkSdkManager manager,String type) {
        if(type.equalsIgnoreCase("chip")){
            Log.d("MPOS","PIN Block : " + DataSetting.getPinBlock(manager));
            Log.d("MPOS","Get Card AID : " + DataSetting.cardAID(manager));
            Log.d("MPOS","Get Application Version Number : " + DataSetting.AppVersionNumber(manager));
            Log.d("MPOS","Get Card Acceptor Terminal Identification : " + DataSetting.TerminalIdentification(manager));
            Log.d("MPOS","Get Merchant Identification : " + DataSetting.CardAcceptorIdentification(manager));
            Log.d("MPOS","Get Transaction Type : " + DataSetting.TransactionType(manager));
            Log.d("MPOS","Get Authorized Amount : " + DataSetting.getAuthorizedAmount(manager));
            Log.d("MPOS","Get Local Time : " + DataSetting.getTransactionTime(manager));
            Log.d("MPOS","Get Card Sequence Number : " + DataSetting.cardSequenceNumber(manager));
            Log.d("MPOS","Track 2 Equivalent Data : " + DataSetting.track2Data(manager));

            int PANSN = PaxConditional.checkPanDigit(manager);

            //call Processing.XML in PAX
            // showMsgBoxFun();
            PAXScreen.showMsgBoxFun(manager);

            JsonObject obj = ApiJsonMap(DataSetting.getAuthorizedAmount(manager),DataSetting.cardSequenceNumber(manager),DataSetting.track2Data(manager),"6B409832",DataSetting.getPinBlock(manager),DataSetting.getAllData(manager),PANSN);

            PosApiInterface posApiInterface = TempApiAuthFactory.getTestClient().create(PosApiInterface.class);
            Call<PosTransResponse> call = posApiInterface.SendTransRequest(obj);
            call.enqueue(new Callback<PosTransResponse>() {
                @Override
                public void onResponse(Call<PosTransResponse> call, Response<PosTransResponse> response) {
                    Log.d("SuccessAPI", String.valueOf(response.body()));
                    if(response.isSuccessful()){
                        PosTransResponse posTransResponse = response.body();
                        String details = posTransResponse.getStatusDes().toString();
                        TransactionResponseHandler.responseHandler(details);
                        String AuthManager = "";
                        Log.d("MPOS","RESPONSE CODE : " + ResponseConstant.DE39);
                        if((ResponseConstant.DE39).equalsIgnoreCase("0000")){
                            AuthManager = "00";
                        }else{
                            AuthManager ="01";
                        }
                        System.out.println(AuthManager);
                        SetResponseCodeRes = setResponseData(manager,ResponseConstant.DE39.substring(2),AuthManager,ResponseConstant.DE38,ResponseConstant.DE55);

                        int result = completeTransAndGetData(manager);

                        if(ResponseConstant.DE39.substring(2).equalsIgnoreCase(String.valueOf(BankResponse.Approved))){
                            Log.d("MPOS",String.valueOf(result));
                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","success");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", "Rupay");
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);
                            startActivity(intent);
                        }
                        else {
                            Intent intent = new Intent(PosActivity.this, TransactionStatusActivity.class);
                            intent.putExtra("flag","failure");
                            intent.putExtra("RRN_NO",ResponseConstant.DE37);
                            intent.putExtra("APP_NAME", "RuPay Debit");
                            intent.putExtra("AID", DataSetting.cardAID(manager));
                            intent.putExtra("AMOUNT", amount);
                            intent.putExtra("MID", ResponseConstant.DE42);
                            intent.putExtra("TID", ResponseConstant.DE41);
                            intent.putExtra("TXN_ID", "1234567");
                            intent.putExtra("INVOICE", "1111111");
                            intent.putExtra("CARD_TYPE", "Rupay");
                            intent.putExtra("APPR_CODE", "123456");
                            intent.putExtra("CARD_NUMBER", ResponseConstant.DE35);

                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void onFailure(Call<PosTransResponse> call, Throwable t) {
                    call.cancel();
                    Log.d("Failure", String.valueOf(t));
                }
            });
        }
        else {
            Toast.makeText(getApplicationContext(),"MSR Transaction Data",Toast.LENGTH_LONG);
        }
    }

    private JsonObject ApiJsonMap(String authorizedAmount, String cardSequenceNumber, String track2Data, String s1, String pinBlock, String allData,Integer panSlNo) {
        JsonObject jsonObject = new JsonObject();
        try{
            JSONObject obj = new JSONObject();
            obj.put("pansn",panSlNo);
            obj.put("amount",authorizedAmount);
            obj.put("cardSequenceNumber",cardSequenceNumber);
            obj.put("trackData",track2Data);
            obj.put("deviceSerial",s1);
            obj.put("pinblock",pinBlock);
            obj.put("deviceData",allData);
            obj.put("transactionType",transaction_type);

            Log.d("MPOS","Request Data : " + obj.toString());
            JsonParser jsonParser = new JsonParser();
            jsonObject = (JsonObject) jsonParser.parse(obj.toString());
        }catch (JSONException e){
            e.printStackTrace();
        }
        return jsonObject;
    }

}
