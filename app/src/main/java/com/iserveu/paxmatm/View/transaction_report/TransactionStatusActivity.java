package com.iserveu.paxmatm.View.transaction_report;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iserveu.paxmatm.R;
import com.iserveu.paxmatm.Utils.PAXScreen;
import com.iserveu.paxmatm.View.Dashboard.MainActivity;
import com.paxsz.easylink.api.EasyLinkSdkManager;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TransactionStatusActivity extends AppCompatActivity {
    private RelativeLayout ll_maiin;
    private TextView statusMsgTxt,statusDescTxt;
    private ImageView status_icon;

    private TextView appl_name,a_id,date_time,rref_num,mid,tid,txnid,invoice,card_id,appr_code,card_no,card_amount;
    public EasyLinkSdkManager manager;

    private Button backBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_status);
        ll_maiin = findViewById(R.id.ll_maiin);
        manager = EasyLinkSdkManager.getInstance(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        statusMsgTxt = findViewById(R.id.statusMsgTxt);
        status_icon = findViewById(R.id.status_icon);
        appl_name = findViewById(R.id.appl_name);
        a_id = findViewById(R.id.a_id);
        rref_num = findViewById(R.id.rref_num);
        mid = findViewById(R.id.mid);
        tid = findViewById(R.id.tid);
        txnid = findViewById(R.id.txnid);
        invoice = findViewById(R.id.invoice);
        card_id = findViewById(R.id.card_id);
        appr_code = findViewById(R.id.appr_code);
        card_no = findViewById(R.id.card_no);
        card_amount = findViewById(R.id.card_amount);
        statusDescTxt = findViewById(R.id.statusDescTxt);
        backBtn = findViewById(R.id.backBtn);
        date_time = findViewById(R.id.date_time);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd : HH.mm.ss");
        String currentDateandTime = sdf.format(new Date());
        date_time.setText(currentDateandTime);



        String applName = getIntent().getStringExtra("APP_NAME");
        String aId = getIntent().getStringExtra("AID");
        String prefNum = getIntent().getStringExtra("RRN_NO");
        String MID = getIntent().getStringExtra("MID");
        String TID = getIntent().getStringExtra("TID");
        String TXN_ID = getIntent().getStringExtra("TXN_ID");
        String INVOICE = getIntent().getStringExtra("INVOICE");

        String CARD_TYPE = getIntent().getStringExtra("CARD_TYPE");
        String APPR_CODE = getIntent().getStringExtra("APPR_CODE");
        String CARD_NUMBER = getIntent().getStringExtra("CARD_NUMBER");
        String AMOUNT = getIntent().getStringExtra("AMOUNT");
        AMOUNT = replaceWithZero(AMOUNT);
        System.out.println(">>>----"+AMOUNT);

        String[] splitAmount = CARD_NUMBER.split("D");
        CARD_NUMBER = splitAmount[0];

        String firstnum = CARD_NUMBER.substring(0,2);
        String middlenum = CARD_NUMBER.substring(2,CARD_NUMBER.length()-2);
        String lastNum = CARD_NUMBER.replace(firstnum+middlenum,"");

        System.out.println(">>>---"+firstnum);
        System.out.println(">>>---"+middlenum);
        System.out.println(">>>---"+lastNum);





        /*CARD_NUMBER = CARD_NUMBER.substring(0, CARD_NUMBER.length() - 2);
        CARD_NUMBER = CARD_NUMBER + "**";*/

      /*  String first2Digit = CARD_NUMBER.substring(0,2);
        String middeleDigit = CARD_NUMBER.substring(2,CARD_NUMBER.length());
        String last2Digit = CARD_NUMBER.substring(0, CARD_NUMBER.length() - 2);*/


        //CARD_NUMBER = first2Digit+middeleDigit.replaceAll("[0-9]","*")+last2Digit;


        String flag = getIntent().getStringExtra("flag");
        if(flag.equalsIgnoreCase("failure")){
            ll_maiin.setBackgroundColor(Color.parseColor("#D94237"));
            status_icon.setImageResource(R.drawable.ic_errorrr);
            statusMsgTxt.setText("Failed.");
            statusDescTxt.setText("UnFortunatly Paymet was rejected.");
            backBtn.setBackgroundResource(R.drawable.button_backgroundtransaction_fail);

            if (Build.VERSION.SDK_INT >= 21) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.error));
            }
            //Show Failure
            PAXScreen.showFailure(manager);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.seconderyDark));
        }
        //Show Success
        PAXScreen.showSuccess(manager);
        appl_name.setText(applName);
        a_id.setText(aId);
        rref_num.setText(prefNum);
        mid.setText(MID);
        tid.setText(TID);
        txnid.setText(TXN_ID);
        invoice.setText(INVOICE);
        card_id.setText(CARD_TYPE);
        appr_code.setText(APPR_CODE);
        card_no.setText(CARD_NUMBER);
        card_amount.setText(AMOUNT);


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TransactionStatusActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });






    }

    public String replaceWithZero(String s) {
        int length = s.length();
        //Check whether or not the string contains at least four characters; if not, this method is useless
        if (length < 2)
            return "Error: The provided string is not greater than four characters long.";
        return s.substring(0, length - 2) + ".00";
    }

/*
    private void showSuccess() {
        ShowPageInfo showPageInfo = new ShowPageInfo("Title1","ABCD");
        int timeout = 10000; // 600 * 100ms
        ArrayList<ShowPageInfo> showPageList = new ArrayList<ShowPageInfo>();
        showPageList.add(showPageInfo);
        manager.showPage ("Success.xml",timeout, showPageList);
    }

    private void showFailure() {
        ShowPageInfo showPageInfo = new ShowPageInfo("Title1","ABCD");
        int timeout = 10000; // 600 * 100ms
        ArrayList<ShowPageInfo> showPageList = new ArrayList<ShowPageInfo>();
        showPageList.add(showPageInfo);
        manager.showPage ("Failure.xml",timeout, showPageList);
    }*/
}
