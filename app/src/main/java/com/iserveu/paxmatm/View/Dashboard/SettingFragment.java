package com.iserveu.paxmatm.View.Dashboard;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.iserveu.paxmatm.Adapters.Settings.SettingRecyclerAdapter;
import com.iserveu.paxmatm.Model.SettingList;
import com.iserveu.paxmatm.R;
import com.iserveu.paxmatm.Service.LoginSession;
import com.iserveu.paxmatm.Service.RecyclerTouchListener;
import com.iserveu.paxmatm.View.Bluetooth.BluetoothActivity;
import com.iserveu.paxmatm.View.Login.LoginActivity;
import com.iserveu.paxmatm.View.MPOS.PosActivity;

import java.util.ArrayList;
import java.util.List;

import static android.support.v4.content.PermissionChecker.checkSelfPermission;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    private List<SettingList> settingLists = new ArrayList<>();
    private RecyclerView recyclerView;
    private SettingRecyclerAdapter settingRecyclerAdapter;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onDestroyView() {
        settingLists = null;
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View settingView =  inflater.inflate(R.layout.fragment_setting, container, false);

        recyclerView = settingView.findViewById(R.id.settinglist);
        settingRecyclerAdapter = new SettingRecyclerAdapter(settingLists);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(settingRecyclerAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                SettingList List = settingLists.get(position);
                if(List.getTitle() == "PAX Bluetooth Pair"){
                    Intent intent = new Intent(getContext(),BluetoothActivity.class);
                    if (checkSelfPermission(getContext(),Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                        Toast.makeText(getContext(),"Please Grant all the permissions",Toast.LENGTH_LONG).show();
                    } else {
                        startActivity(intent);
                    }

                }
                else if(List.getTitle() == "Logout"){
                    logout();
                }
                else if(List.getTitle() == "Download"){
                    //logout();
                    Intent intent = new Intent(getActivity(),FileDownLoadActivity.class);
                    startActivity(intent);
                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareItem();

        return settingView;
    }

    private void prepareItem() {
        SettingList Ble = new SettingList(R.drawable.ic_bluetooth_logo,"PAX Bluetooth Pair");
        SettingList Logout = new SettingList(R.drawable.ic_home_black_24dp,"Logout");
        SettingList fileDownload = new SettingList(R.drawable.ic_download,"Download");
        settingLists.add(Ble);
        settingLists.add(Logout);
        settingLists.add(fileDownload);
        settingRecyclerAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(settingRecyclerAdapter);
    }

    private void logout(){
        LoginSession.clearData(getContext());
        /*Intent loginIntent = new Intent(getContext(), LoginActivity.class);
        startActivity(loginIntent);*/

        Intent loginIntent = new Intent(getActivity(), LoginActivity.class);
        loginIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        getActivity().startActivity(loginIntent);
    }

}
