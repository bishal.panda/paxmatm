package com.iserveu.paxmatm.View.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.iserveu.paxmatm.R;
import com.iserveu.paxmatm.Service.LoginSession;
import com.iserveu.paxmatm.Utils.EnvData;
import com.iserveu.paxmatm.Utils.JWTUtils;
import com.iserveu.paxmatm.View.Dashboard.MainActivity;
import com.iserveu.paxmatm.ViewModel.LoginViewModel;
import com.iserveu.paxmatm.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {

    ProgressBar loading;
    public static LoginActivity loginActivity;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginActivity = this;
        loading = findViewById(R.id.progressBar);

        ActivityLoginBinding activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.setViewModel(new LoginViewModel());
        activityLoginBinding.executePendingBindings();
        LoginViewModel.getResponse().observe(this, loginResponse -> {
            try {
                EnvData.token = loginResponse.getToken();
                EnvData.AdminName = loginResponse.getAdmin();
                Log.d("Data",EnvData.token);
                LoginSession.storeData(loginResponse.getToken(),loginResponse.getAdmin(),getApplicationContext());
                JWTUtils.decoded(EnvData.token);
                stopProgressDialog();
                Intent dashboardIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(dashboardIntent);
            } catch (Exception e) {
                stopProgressDialog();
                Toast.makeText(getApplicationContext(),"Are you sure you have entered the correct Username or Password",Toast.LENGTH_LONG).show();
            }

        });
        LoginViewModel.getBusy().observe(this, status -> {
            if(status == 0){
                loading.setVisibility(View.VISIBLE);
            }else{
                loading.setVisibility(View.GONE);
            }
        });

    }

    public void runProgressDialog(){
        dialog = new ProgressDialog(LoginActivity.this,R.style.MyTheme);
        dialog.setCancelable(false);
        dialog.setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        dialog.show();
        ProgressBar progressbar=(ProgressBar)dialog.findViewById(android.R.id.progress);
        progressbar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#C60000"), android.graphics.PorterDuff.Mode.SRC_IN);

    }
    public void stopProgressDialog(){
        if(dialog!=null) {
            dialog.dismiss();
        }
    }

    public void showInvalidUser(){
        Toast.makeText(getApplicationContext(),"Are you sure you have entered the correct Username or Password",Toast.LENGTH_LONG).show();
    }


}
