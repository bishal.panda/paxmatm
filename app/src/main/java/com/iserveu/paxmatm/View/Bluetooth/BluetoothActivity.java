package com.iserveu.paxmatm.View.Bluetooth;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.iserveu.paxmatm.R;
import com.iserveu.paxmatm.Service.Snackbar;
import com.iserveu.paxmatm.Utils.Dialog;
import com.iserveu.paxmatm.Utils.ResultEvent;
import com.iserveu.paxmatm.View.Dashboard.MainActivity;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.api.ResponseCode;
import com.paxsz.easylink.device.DeviceInfo;
import com.paxsz.easylink.listener.SearchDeviceListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.content.ContentValues.TAG;

public class BluetoothActivity extends AppCompatActivity{

    ConstraintLayout pairedDeviceLayout;
    TextView pairedDeviceTitle;
    ListView availableDevicesList;
    CoordinatorLayout container;

    private static final String DEVICE_NAME = "DEVICE_NAME";
    private static final String DEVICE_MAC = "DEVICE_MAC";

    private String pairedDeviceName;
    private String pairedDeviceMac;

    private ImageView refresh;
    private ImageView back;
    private RotateAnimation anim;
    private ArrayList<HashMap<String, String>> deviceInfo = new ArrayList<>();
    private EasyLinkSdkManager manager;
    private Handler handler;
    private ExecutorService backgroundExecutor;

    private AlertDialog alertDialog;
    private boolean isStopBtn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        pairedDeviceLayout = findViewById(R.id.pairedDeviceLayout);
        pairedDeviceTitle = findViewById(R.id.pairedDeviceTitle);
        availableDevicesList = findViewById(R.id.availableDevicesList);
        container = findViewById(R.id.container);

        manager = EasyLinkSdkManager.getInstance(this);
        handler = new Handler();
        backgroundExecutor = Executors.newFixedThreadPool(10, runnable -> {
            Thread thread = new Thread(runnable, "Background executor service");
            thread.setPriority(Thread.MIN_PRIORITY);
            thread.setDaemon(true);
            return thread;
        });

        register(this);
        initView();
        setListener();

        refresh.setOnClickListener(v -> {
            refresh.startAnimation(anim);
            deviceInfo.clear();
            System.out.println(">>-----"+deviceInfo.size());
            updateBluetoothList();
            manager.searchDevices(new CustomDeviceSearchListener(), 10000);
        });

        back.setOnClickListener(view -> onBackPressed());


    }

    public void doEvent(ResultEvent event) {
        EventBus.getDefault().post(event);
    }

    public void runInBackground(final Runnable runnable) {
        backgroundExecutor.submit(runnable);
    }

    private void initView() {


        getSupportActionBar().setTitle("Bluetooth");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        this.getSupportActionBar().setDisplayShowCustomEnabled(true);
        this.getSupportActionBar().setCustomView(R.layout.custom_ble_actionbar);
        View view = getSupportActionBar().getCustomView();

        resetPairedDeviceUI();

        refresh = ((View) view).findViewById(R.id.refresh);
        back = findViewById(R.id.back);
        anim = new RotateAnimation(360.0f, 0.0f , Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(1000);

        refresh.setAnimation(anim);
        deviceInfo.clear();
        updateBluetoothList();
        manager.searchDevices(new CustomDeviceSearchListener(), 40000);
    }

    private void setListener() {
        availableDevicesList.setOnItemClickListener((parent, arg1, pos, id) -> onItemClick(pos));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(ResultEvent event) {
        switch ((ResultEvent.Status) event.getStatus()) {
            case DISCOVER_ONE:
                addDevice((DeviceInfo) event.getData());
                break;
            case SEARCH_COMPLETE:
                refresh.clearAnimation();
                break;
            case CONNECT_SUCCESS:
                refresh.clearAnimation();
                dialogDismiss();
                resetPairedDeviceUI();
                updateBluetoothList();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                break;
            case CONNECT_FAILED:
                refresh.clearAnimation();
                dialogDismiss();
                resetPairedDeviceUI();
                updateBluetoothList();
                Snackbar.showSnackBar(container, ResponseCode.getRespCodeMsg((int) event.getData()));
                break;
            default:
                break;
        }
    }

    private void resetPairedDeviceUI() {
        if (!manager.isConnected(DeviceInfo.CommType.BLUETOOTH)) {
            pairedDeviceLayout.setVisibility(View.GONE);
            if (pairedDeviceName != null && pairedDeviceMac != null) {
                HashMap<String, String> map = new HashMap<>();
                map.put(DEVICE_NAME, pairedDeviceName);
                map.put(DEVICE_MAC, pairedDeviceMac);
                deviceInfo.add(map);
                pairedDeviceName = "";
                pairedDeviceMac = "";
            }
            return;
        }
        pairedDeviceName = manager.getConnectedDevice().getDeviceName();
        pairedDeviceMac = manager.getConnectedDevice().getIdentifier();
        pairedDeviceLayout.setVisibility(View.VISIBLE);
        pairedDeviceTitle.setText(pairedDeviceName);

        //remove paired device from available devices list
        for (int i = 0; i < deviceInfo.size(); i++) {
            if (pairedDeviceName.equals(deviceInfo.get(i).get(DEVICE_NAME))
                    && pairedDeviceMac.equals(deviceInfo.get(i).get(DEVICE_MAC))) {
                deviceInfo.remove(i);
                break;
            }
        }
    }

    private void updateBluetoothList() {
        final BluetoothListAdapter adapter = new BluetoothListAdapter(this, deviceInfo);
        availableDevicesList.setAdapter(adapter);
        availableDevicesList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    private void onItemClick(int pos) {
        alertDialog = new AlertDialog.Builder(this).create();
        Dialog.showProgressDialog(this, alertDialog, getString(R.string.prompt_connecting),
                getString(R.string.prompt_wait), false);

        String currentName = deviceInfo.get(pos).get(DEVICE_NAME);
        String currentMac = deviceInfo.get(pos).get(DEVICE_MAC);
        saveDeviceInfo(currentName,currentMac);
        manager.stopSearchingDevice();
        runInBackground(() -> connectDevice(currentName, currentMac));
    }

    private void saveDeviceInfo(String currentName,String currentMac){
        SharedPreferences pref = getSharedPreferences("AuthData", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("DEVICE_NAME",currentName);
        editor.putString("DEVICE_MAC",currentMac);
        editor.commit();
    }
    private void connectDevice(String deviceName, String deviceMac) {
        DeviceInfo deviceInfo = new DeviceInfo(DeviceInfo.CommType.BLUETOOTH, deviceName, deviceMac);
        int ret = manager.connect(deviceInfo);
        if (isDestroyed()) {
            return;
        }
        if (ret == ResponseCode.EL_RET_OK) {
            Log.i("log", "connect success");
            doEvent(new ResultEvent(ResultEvent.Status.CONNECT_SUCCESS));
        } else {
            doEvent(new ResultEvent(ResultEvent.Status.CONNECT_FAILED, ret));
        }
    }

    private class CustomDeviceSearchListener implements SearchDeviceListener {
        @Override
        public void discoverOneDevice(DeviceInfo deviceInfo) {
            doEvent(new ResultEvent(ResultEvent.Status.DISCOVER_ONE, deviceInfo));
        }

        @Override
        public void discoverComplete() {
            doEvent(new ResultEvent(ResultEvent.Status.SEARCH_COMPLETE));
        }
    }

    public class BluetoothListAdapter extends BaseAdapter {

        private Context context;

        public BluetoothListAdapter(Context context, ArrayList<HashMap<String, String>> deviceInfo) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return deviceInfo.size();
        }

        @Override
        public Object getItem(int position) {
            return deviceInfo.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = LayoutInflater.from(context).inflate(R.layout.bluetooth_list, parent, false);
            } else {
                view = convertView;
            }
            TextView tv = (TextView) view.findViewById(R.id.bluetoothDeviceName);// display text
            ImageView imageView = (ImageView) view.findViewById(R.id.image);
            if(deviceInfo.size()>0){
                tv.setText(deviceInfo.get(position).get(DEVICE_NAME));
                imageView.setImageResource(R.drawable.icn_bluetooth);}
            return view;
        }
    }


    private void addDevice(DeviceInfo deviceInf) {
        //avoid add the same device
        if (deviceInf.getDeviceName().equals(pairedDeviceName)
                && deviceInf.getIdentifier().equals(pairedDeviceMac)) {
            return;
        }
        for (int i = 0; i < deviceInfo.size(); i++) {
            if (deviceInf.getDeviceName().equals(deviceInfo.get(i).get(DEVICE_NAME))
                    && deviceInf.getIdentifier().equals(deviceInfo.get(i).get(DEVICE_MAC))) {
                return;
            }
        }

        HashMap<String, String> map = new HashMap<>();
        map.put(DEVICE_NAME, deviceInf.getDeviceName());
        map.put(DEVICE_MAC, deviceInf.getIdentifier());
        deviceInfo.add(map);
        updateBluetoothList();
    }

    private void dialogDismiss() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    public void unregister(Object obj) {
        if (EventBus.getDefault().isRegistered(obj)) {
            EventBus.getDefault().unregister(obj);
        }
    }

    public void register(Object obj) {
        if (!EventBus.getDefault().isRegistered(obj)) {
            EventBus.getDefault().register(obj);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1001) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "requestPermissions success");
            } else {
                Log.d(TAG, "requestPermissions fail");
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregister(this);
    }



}
