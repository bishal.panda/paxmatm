package com.iserveu.paxmatm.View.Splash;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.iserveu.paxmatm.R;
import com.iserveu.paxmatm.Service.LoginSession;
import com.iserveu.paxmatm.View.Dashboard.MainActivity;
import com.iserveu.paxmatm.View.Login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    ImageView logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        logo = findViewById(R.id.splashLogo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        try {
            if(LoginSession.sessionCheck(getApplicationContext())){
                new Handler().postDelayed(()->{
                    Intent sharedActivity = new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(sharedActivity);
                    finish();
                },SPLASH_TIME_OUT);
            }
            else{
                new Handler().postDelayed(() -> {
                    Intent sharedActivity = new Intent(SplashActivity.this, LoginActivity.class);

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        Pair[] pairs = new Pair[1];
                        pairs[0] = new Pair(logo,"imageTransaction");
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(SplashActivity.this,pairs);
                        startActivity(sharedActivity,options.toBundle());
                    }
                    else {
                        startActivity(sharedActivity);
                    }

                    // close this activity
                    finish();
                }, SPLASH_TIME_OUT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
