package com.iserveu.paxmatm.View.Error;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserveu.paxmatm.R;
import com.iserveu.paxmatm.View.Dashboard.MainActivity;
import com.iserveu.paxmatm.View.transaction_report.TransactionStatusActivity;

public class ErrorActivity extends AppCompatActivity implements View.OnClickListener {

    TextView error_txt;
    ImageView closeView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        int errorInt  = getIntent().getIntExtra("errorResponse",0);
        error_txt = findViewById(R.id.error_txt);
        closeView =findViewById(R.id.closeView);
        closeView.setOnClickListener(this);
        switch (errorInt) {
            case 4046:
                error_txt.setText("CardExpire");
                break;
            case 4001:
                error_txt.setText("BlockCard");
                 break;
            case 4003:
                error_txt.setText("BlockApplication");
                break;
            case 4006:
                error_txt.setText("UnknownAID");
                break;
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeView:
                Intent i = new Intent(ErrorActivity.this, MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
                break;
        }
    }
}
