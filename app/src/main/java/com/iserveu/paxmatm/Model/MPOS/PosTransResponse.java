package com.iserveu.paxmatm.Model.MPOS;

import com.google.gson.annotations.SerializedName;

public class PosTransResponse {


    @SerializedName("status")
    private Integer status;
    @SerializedName("statusDes")
    private String statusDes;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStatusDes() {
        return statusDes;
    }

    public void setStatusDes(String statusDes) {
        this.statusDes = statusDes;
    }
}
