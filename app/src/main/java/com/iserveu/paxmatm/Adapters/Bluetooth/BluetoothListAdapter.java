package com.iserveu.paxmatm.Adapters.Bluetooth;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserveu.paxmatm.R;
import com.paxsz.easylink.device.DeviceInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BluetoothListAdapter extends BaseAdapter {

    private Context context;
    private List<DeviceInfo> deviceList;

    public BluetoothListAdapter(Context context, ArrayList<HashMap<String, String>> deviceInfo) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return deviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return deviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.bluetooth_list, parent, false);
        } else {
            view = convertView;
        }
        TextView tv = (TextView) view.findViewById(R.id.title);// display text
        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        tv.setText(deviceList.get(position).getDeviceName());
        imageView.setImageResource(R.drawable.icn_bluetooth);
        return view;
    }
}
