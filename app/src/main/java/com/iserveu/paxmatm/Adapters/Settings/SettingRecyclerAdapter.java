package com.iserveu.paxmatm.Adapters.Settings;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.iserveu.paxmatm.Model.SettingList;
import com.iserveu.paxmatm.R;

import java.util.List;

public class SettingRecyclerAdapter extends RecyclerView.Adapter<SettingRecyclerAdapter.MyViewHolder> {

    private final List<SettingList> settingList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView icon;
        
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.settingName);
            icon = itemView.findViewById(R.id.settingIcon);
        }
    }
    public SettingRecyclerAdapter(List<SettingList>settingList){
        this.settingList=settingList;
    }

    @Nullable
    @Override
    public SettingRecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.setting_list,viewGroup,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        SettingList List = settingList.get(position);
        myViewHolder.title.setText(List.getTitle());
        myViewHolder.icon.setImageResource(List.getImageId());
    }
    @Override
    public int getItemCount() {
        return settingList.size();
    }
}
