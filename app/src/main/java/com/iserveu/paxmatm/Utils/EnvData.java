package com.iserveu.paxmatm.Utils;

import com.iserveu.paxmatm.Model.Dashboard.UserProfile;

public class EnvData {
    public static String token = "";
//    public static final String BASE_URL = "http://35.244.32.110:8085";
    public static final String BASE_URL = "https://iserveu.online";
    public static final String TEST_URL = "http://35.200.141.19:8080";

    public static String AdminName = "";
    public static String UserName = "";
    public static String UserBalance = "";
    public static String UserType = "";
    public String hexAmount = "";
    public static UserProfile userProfile;


    //Constant Data for MPOS
    public static String Transaction_Type = "9C0101";
    public static String Transaction_Currency_Code = "5F2A020356";
    public static String CurrencyExponent = "5F360102";
    public static String AmountOthers = "9F0306000000000000";
    public static String PINEncryptionType = "02020101";
    public static String PINEncryptionKeyIdx = "02030120";
    public static String DataEncryptionType = "02050101";
    public static String DataEncryptionKeyIdx = "02060125";
    public static String PINBlockMode = "02040100";
    public static String TransactionProcessingMode = "02090100";
    public static String CardEntryMode = "02140102";
    public static String CardEntryModeFallBack = "02140107";
    public static String FallbackAllowFlag = "02070101";


    public void setHexAmount(String amount){
        hexAmount = amount;
    }

    public String getHexAmount(){
        return hexAmount;
    }



}
