package com.iserveu.paxmatm.Utils;

import android.os.AsyncTask;

import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.model.ShowPageInfo;

import java.util.ArrayList;

public class PAXScreen {

    public static void showSuccess(EasyLinkSdkManager manager) {

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //code you want to run on the background
                ShowPageInfo showPageInfo = new ShowPageInfo("Title1","ABCD");
                int timeout = 6000; // 600 * 100ms
                ArrayList<ShowPageInfo> showPageList = new ArrayList<ShowPageInfo>();
                showPageList.add(showPageInfo);
                manager.showPage ("Success.xml",timeout, showPageList);
            }
        });
    }

    public static void showFailure(EasyLinkSdkManager manager) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //code you want to run on the background
                ShowPageInfo showPageInfo = new ShowPageInfo("Title1","ABCD");
                int timeout = 6000; // 600 * 100ms
                ArrayList<ShowPageInfo> showPageList = new ArrayList<ShowPageInfo>();
                showPageList.add(showPageInfo);
                manager.showPage ("Failure.xml",timeout, showPageList);
            }
        });

    }


    public static void showMsgBoxFun(EasyLinkSdkManager manager) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //code you want to run on the background
                ShowPageInfo showPageInfo = new ShowPageInfo("Title1", "ABCD");
                int timeout = 60000; // 600 * 100ms
                ArrayList<ShowPageInfo> showPageList = new ArrayList<ShowPageInfo>();
                showPageList.add(showPageInfo);
                manager.showPage("Processing.xml", timeout, showPageList);
            }
        });
    }



        public static void showErrorOnPax(EasyLinkSdkManager manager,int errorCode) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    //code you want to run on the background
                    ShowPageInfo showPageInfo = new ShowPageInfo("Title1","ABCD");
                    int timeout = 6000; // 600 * 100ms
                    ArrayList<ShowPageInfo> showPageList = new ArrayList<ShowPageInfo>();
                    showPageList.add(showPageInfo);
                    switch (errorCode) {
                        case 4046:
                            manager.showPage ("CardExpire.xml",timeout, showPageList);
                            break;
                        case 4001:
                            manager.showPage ("BlockCard.xml",timeout, showPageList);
                            break;
                        case 4003:
                            manager.showPage ("BlockApplication.xml",timeout, showPageList);
                            break;
                        case 4006:
                            manager.showPage ("UnknownAID.xml",timeout, showPageList);
                            break;
                    }
                }
            });

        }

    }

