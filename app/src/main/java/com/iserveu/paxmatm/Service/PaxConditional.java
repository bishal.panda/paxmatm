package com.iserveu.paxmatm.Service;

import android.util.Log;

import com.iserveu.paxmatm.Utils.Tools;
import com.paxsz.easylink.api.EasyLinkSdkManager;
import com.paxsz.easylink.api.ResponseCode;
import com.paxsz.easylink.model.DataModel;

import java.io.ByteArrayOutputStream;

public class PaxConditional {
    public static Integer checkPanDigit(EasyLinkSdkManager manager){
        Integer response = null;
        String panNumber;
        ByteArrayOutputStream getPanNumber = new ByteArrayOutputStream();
        response = manager.getData(DataModel.DataType.TRANSACTION_DATA,new byte[]{(byte)0x5F, 0x34},getPanNumber);
        if(response == ResponseCode.EL_RET_OK){
            panNumber = Tools.cleanByte(Tools.bcd2Str(getPanNumber.toByteArray()));
            panNumber = panNumber.substring(6);
            Log.d("MPOS","Pan Sequence Number :" + panNumber);
            if(Integer.parseInt(panNumber) == 15){
                return 15;
//                String CID;
//                ByteArrayOutputStream getCID = new ByteArrayOutputStream();
//                response = manager.getData(DataModel.DataType.TRANSACTION_DATA,new byte[]{(byte)0x9F, 0x27},getCID);
//                CID = Tools.cleanByte(Tools.bcd2Str(getCID.toByteArray()));
//                CID = CID.substring(6);
//                Log.d("MPOS","CID :" + CID);
            }
            else {
                return 1;
            }
        }
        else{
            return 0;
        }
    }
}
