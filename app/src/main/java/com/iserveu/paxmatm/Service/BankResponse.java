package com.iserveu.paxmatm.Service;

public class BankResponse {
    public static String Approved = "00";
    public static String InvalidMerchant = "03";
    public static String PickUp = "04";
    public static String DO_NOT_HONOR = "05";
    public static String Error = "06";
    public static String Invalid_transaction = "12";
    public static String Invalid_amount = "13";
    public static String Invalid_card_number = "14";

    public static String No_such_issuer = "15";
    public static String Customer_cancellation = "17";
    public static String Invalid_response = "20";
    public static String No_action_taken = "21";
    public static String Suspected_malfunction = "22";
    public static String Unable_to_locate_record = "25";
    public static String File_Update_field_edit_error = "27";

    public static String Record_already_exist_in_the_file = "28";

    public static String File_Update_not_successful = "29";

    public static String Format_error = "30";
    public static String Bank_not_supported_by_switch = "31";
    public static String Expired_card_capture = "33";

    public static String Suspected_fraud = "34";
    public static String Restricted_card = "36";
    public static String Allowable_PIN = "38";
    public static String No_credit_account = "39";
    public static String Requested_functio = "40";
    public static String Lost_card = "41";
    public static String No_universal_account = "42";

    public static String Stolen_card = "43";
    public static String Not_sufficient_funds = "51";
    public static String No_checking_account = "52";
    public static String No_savings_account = "53";
    public static String Expired_card_decline = "54";
    public static String Incorrect_personal_identification_number = "55";
    public static String No_card_record = "56";
    public static String Transaction_not_permitted_to_Cardholder = "57";
    public static String Suspected_fraud_decline = "59";
    public static String Card_acceptor_contact_acquirer = "60";
    public static String Exceeds_withdrawal_amount_limit = "61";
    public static String Restricted_Card_decline ="62";
    public static String Security_violation = "63";


    public static String Exceeds_withdrawal_frequency = "65";
    public static String Card_acceptor_calls_acquirer = "66";
    public static String Hard_capture = "67";
    public static String Acquirer_time = "68";
    public static String Mobile_number_record_not_found = "69";
    public static String Deemed_Acceptance = "71";
    public static String Transactions_declined_by_Issuer = "74";
    public static String Allowable_number_of_PIN = "75";

    public static String Cryptographic_Error = "81";
    public static String Cut_off_is_in_process = "90";
    public static String Issuer_or_switch_is_inoperative = "91";
    public static String No_routing_available = "92";

    public static String Transaction_cannot_be_completed = "93";
    public static String Duplicate_transmission = "94";
    public static String Reconcile_error = "95";

    public static String ARQC_validation_failed = "E3";
    public static String TVR_validation_failed = "E4";

    public static String CVR_validation_failed_by_Issuer = "E5";
    public static String No_Aadhar_linked_to_Card = "MU";
    public static String INVALID_BIOMETRIC_DATA = "UG";

    public static String BIOMETRIC_DATA_DID_NOT_MATCH = "U3";
    public static String Technical_Decline_UIDAI = "WZ";


    public static String Compliance_error_code_for_issuer = "C1";
    public static String Compliance_error_code_for_acquirer = "CA";
    public static String Compliance_error_code_for_LMM = "M6";
    public static String E_commerce_decline = "ED";
    public static String Approved_or_Completed_successfully = "00";
    public static String System_malfunction = "96";
    public static String Timeout = "91";
    public static String Acquirer_received_ATM_only = "21";
    public static String Acquirer_received = "22";
    public static String Message_edit_failure_during_response_processing_at_NPCI = "CI";
    public static String Acquirer_time_out = "68";
    public static String Customer_cancellation_for_void = "17";
    public static String AAC_GENERATED = "E1";
    public static String Terminal_does_not_receive_AAC_AND_TC = "E2";
    public static String Partial_Reversal = "32";








}
