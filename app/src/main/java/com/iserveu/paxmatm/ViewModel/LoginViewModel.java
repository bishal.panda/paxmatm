package com.iserveu.paxmatm.ViewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.text.TextUtils;
import android.util.Log;

import com.iserveu.paxmatm.Interface.Login.LoginApiInterface;
import com.iserveu.paxmatm.Model.LoginResponse;
import com.iserveu.paxmatm.Model.User;
import com.iserveu.paxmatm.Service.ApiFactory;
import com.iserveu.paxmatm.View.Login.LoginActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {

    private ViewModel model;



    // Defining the POJO Class
    private User user;
    private static MutableLiveData<Integer> busy;
    private static MutableLiveData<LoginResponse> responseData;

    public static MutableLiveData<Integer> getBusy() {
        if (busy == null) {
            busy = new MutableLiveData<>();
            busy.setValue(8);
        }
        return busy;
    }

    public static LiveData<LoginResponse> getResponse(){
        if(responseData == null){
            responseData = new MutableLiveData<>();
        }
        return responseData;
    }

    public void setUserName(String username) {
        user.setUserName(username);
    }

    public void setUserPassword(String password) {
        user.setPassword(password);
    }

    public String getUserName() {
        return user.getUserName();
    }

    public String getUserPassword() {
        return user.getPassword();
    }

    public LoginViewModel() {
        user = new User("","");
    }

    public void onLoginClicked() {
        if (!isInputDataValid()) {
            LoginActivity.loginActivity.showInvalidUser();
        } else {
            LoginActivity.loginActivity.runProgressDialog();

            busy.postValue(0);
            busy.setValue(0);

            LoginApiInterface loginApiService = ApiFactory.getClient().create(LoginApiInterface.class);
            com.iserveu.paxmatm.Model.User user = new User(getUserName(),getUserPassword());
            Call<LoginResponse> call = loginApiService.doLogin(user);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    busy.postValue(8); //8 == View.GONE
                    busy.setValue(8);
                    Log.d("Success", String.valueOf(response.body()));
                    LoginResponse loginResponse = response.body();
                    responseData.postValue(loginResponse);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    call.cancel();
                    Log.d("Failure", String.valueOf(t));
                    busy.postValue(8); //8 == View.GONE
                    busy.setValue(8);
                }
            });

        }
    }

    public boolean isInputDataValid() {
        return !TextUtils.isEmpty(getUserName()) && getUserPassword().length() > 2;
    }


}
