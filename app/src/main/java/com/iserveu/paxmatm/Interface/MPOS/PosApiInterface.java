package com.iserveu.paxmatm.Interface.MPOS;

import com.google.gson.JsonObject;
import com.iserveu.paxmatm.Model.MPOS.PosTransResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PosApiInterface {
    @Headers({
            "content-type: application/json;charset=UTF-8"
    })
    @POST("/IttyMApp/doCashWithdral")
    Call<PosTransResponse> SendTransRequest(@Body JsonObject posTransRequest);
}
