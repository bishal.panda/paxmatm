package com.iserveu.paxmatm.Interface.Login;

import com.iserveu.paxmatm.Model.LoginResponse;
import com.iserveu.paxmatm.Model.User;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginApiInterface {
    @POST("/getlogintoken")
    Call<LoginResponse> doLogin(@Body User user);
}
